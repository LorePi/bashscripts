#!/usr/bin/env node

require('../node_modules/shelljs/global');
var program = require('../node_modules/commander');

var platforms = ['android', 'ios', 'winphone'];
var outputZipPlatform = './outputZipPlatform';

function existsPlatform(val){
  val = val.split(',');
  for(var i = 0, length1 = val.length; i < length1; i++){
    if(platforms.indexOf(val[i]) < 0){
      console.log('This platform: `'+val[i]+'` doesn\'t exist.');
      process.exit(1);
    } 
  }
  return val;
}

program
.option('-p, --platforms [value]', 'Add platforms', existsPlatform)
.arguments('<zipname>')
.action(function (zipname){  
  program.zipname = zipname;
})
.parse(process.argv);

if(!program.platforms){
  console.log('No platform specified.');
  process.exit(1);
}
if(!program.zipname || program.zipname.length <= 0){
  console.log('No zip name specified.');
  process.exit(1);
}

if(!test('-d', outputZipPlatform)){
  mkdir(outputZipPlatform);
}

for(var i = 0, length1 = program.platforms.length; i < length1; i++){

  var filename = program.platforms[i]+'_'+program.zipname+'.zip';
  var excludeFileAndDirectories = ['./zipPlatform\\*', outputZipPlatform+'\\*'];

  ls('./resources').forEach(function(file, index){
    if(program.platforms[i] != file){
      excludeFileAndDirectories.push('./resources/'+file+'\\*');
    }
  });

  if(test('-f', outputZipPlatform+'/'+filename)){
    rm(outputZipPlatform+'/'+filename);
  }

  exec('zip -r '+outputZipPlatform+'/'+filename+' ./ -x '+excludeFileAndDirectories.join(' '), {silent: true, async: false});
  cd('./zipPlatform/'+program.platforms[i]);
  exec('zip -r ../../'+outputZipPlatform+'/'+filename+' ./*', {silent: true, async: false});
  cd('../../');
  console.log('Zip eseguito per la piattaforma: '+program.platforms[i]);
  console.log('Nome del file zip: '+filename);
}

process.exit(0);